extern crate cfg_if;
extern crate wasm_bindgen;

mod utils;

use cfg_if::cfg_if;
use wasm_bindgen::prelude::*;
use web_sys::{Request, Response};
use url::Url;

const SITE: &'static str = include_str!("../static/index.html");

cfg_if! {
    // When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
    // allocator.
    if #[cfg(feature = "wee_alloc")] {
        extern crate wee_alloc;
        #[global_allocator]
        static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;
    }
}

#[wasm_bindgen]
pub fn page(req: Request) -> Response {
    let mut url = Url::parse(&req.url()).unwrap();
    match url.scheme() {
        "http" => {
            url.set_scheme("https").unwrap();
            Response::redirect(url.as_str()).unwrap()
        },
        _ => { let res = Response::new_with_opt_str(Some(SITE)).unwrap();
               res.headers().append("Content-Type", "text/html").unwrap();
               res
        }
    }
}
